// Define the function 'primeDecomposition' below this line
















// Define the function 'primeDecomposition' above this line
// Don't modify anything below this line

function nextPrime(n) {
	let m = nextOdd(n);
	while (!isPrime(m)) {
		m = m + 2;
	}
	return m;
}

function isPrime(n) {
	if (n == 2 || n == 3 || n == 5) {
		return true;
	}
	if (n % 2 == 0 || n % 3 == 0) {
		return false;
	}
	let d = 1;
	let limit = Math.sqrt(n);
	while (6 * d <= limit) {
		if (n % (6 * d + 1) == 0 || n % (6 * d + 5) == 0) {
			return false;
		}
		d++;
	}
	return true;
}

function nextOdd(n) {
	return n + (n % 2) + 1;
}

self.addEventListener("message", (e) => {
	if (e.data[0] == "start") {
		let result = computeOutput(e.data[1]);
		self.postMessage(result);
	}
});

function computeOutput(number) {
	let output = "";
	let factorList = primeDecomposition(number);
	for (let factor of factorList) {
		output += factor + " ";
	}
	console.log(output);
	return output;
}
