In this assignment, you will implement an algorithm.

Quick links:

-   [How to start an assignment](https://youtu.be/r99anCIBMeU)
-   [Run the program (after clicking on `Go Live` in VSCode)](http://localhost:9000)
-   [How to submit an assignment](https://youtu.be/rkIN7srfthI)


# Statement

Describe an algorithm that computes the prime factors of a number, and define a function called `primeDecomposition` that executes that algorithm to return an array filled up with the prime factors of the number given as input to the function.

First, write the description inside the code itself using a multiline comment. Multiline comments start with `/*` and end with `*/`. The description should have these sections:

-   Input
-   Output
-   Short description
-   Steps

For example:

```javascript
/*
  Input: blah, blah,...

  Output: blah, blah,...

  Short description: blah, blah,...

  Steps:
  1. blah, blah,...
  2. blah, blah,...
  ...
*/
```

Second, implement the algorithm, writing a function that receives a number as argument and returns an array with the prime divisors of that number.

![Watch the expected result](./assignment19-algorithms1.mp4)

You are given a function `nextPrime` that returns the next prime greater than the number passed as argument. For example, `nextPrime(19)` is `23`.

You are also given a web page that allows the user to enter a number, and that will call the function you define, and show the results.

You only need to write the `primeDecomposition` function definition.


# How to start the assignment

Watch and follow the steps in the next tutorial (but choose the right assignment number, which might be different from the one you see in the video):

[How to start an assignment](https://youtu.be/r99anCIBMeU)

To sum up, the steps are:

1.  Fork the project in your GitLab account.
2.  Clone your copy of the project in VSCode.


# Submission

Follow these steps to submit your assignment:

[How to submit an assignment](https://youtu.be/rkIN7srfthI)

To sum up, the steps are:

1.  In VSCode, click on the `Source control` button in the left panel.
2.  Write a message in the box (anything you want to identify the version you are uploading).
3.  Click on the `Commit` button.
4.  Click on the `Sync Changes` button.
5.  Open your `Documents` folder in Windows File Explorer.
6.  Right-click on the assignment&rsquo;s folder, select `Send to`, and then select `Compressed (zipped) folder`.
7.  In Schoology, click on the `Submit assignment` button, and, in the `Upload` tab, press the button with a document icon, to attach the zip file you have just created in the previous step.
8.  Go back to your `Documents` folder, and delete the compressed file.

After submission of the assignment, you can watch your program running online at this address (replace *user* with your GitLab username)

<https://user.pages.gototic.com/assignment19-algorithms1>


# Hint

To find the prime factors of a number, compute the remainder of the division of all prime numbers starting at 2. Whenever you find a divisor, add it to the list of prime factors, replace the number with the quotient of the division, and continue while the quotient is greater than 1. If the prime number you are checking is not a divisor of the number, then replace it with the next prime and repeat.

The algorithm is similar to the one that checks if a number is prime. There are two main differences here. First, you only need to check prime numbers. That&rsquo;s why the `nextPrime` function comes in handy: instead of increasing the divisor, get the new divisor out of that function like this:

```javascript
d = nextPrime(d);
```

Secondly, when you find a factor, the algorithm doesn&rsquo;t finish, but it adds the divisor to the list of factors. And you will only advance the divisor when the current one doesn&rsquo;t divide the number.
