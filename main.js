function openModal($el) {
	$el.classList.add("is-active");
}

function closeModal($el) {
	$el.classList.remove("is-active");
}

function closeAllModals() {
	(document.querySelectorAll(".modal") || []).forEach(($modal) => {
		closeModal($modal);
	});
}

(document.querySelectorAll(".js-modal-trigger") || []).forEach(
	($trigger) => {
		const modal = $trigger.dataset.target;
		const $target = document.getElementById(modal);

		$trigger.addEventListener("click", () => {
			openModal($target);
		});
	},
);

(
	document.querySelectorAll(
		".modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button",
	) || []
).forEach(($close) => {
	const $target = $close.closest(".modal");

	$close.addEventListener("click", () => {
		closeModal($target);
	});
});

document.addEventListener("keydown", (event) => {
	if (event.key === "Escape") {
		closeAllModals();
	}
});

let algo = new Worker('script.js');

let elapsed = 0;

let timer = null;
algo.addEventListener('message', (e) => {
	console.log("Work done");
	console.log(e.data);
	output.value = e.data;
	writeOutput(e.data);
	clearInterval(timer);
});

let input = document.getElementById("num");
let button = document.getElementById("runAlgorithm");
let output = document.getElementById("output");
button.addEventListener("click", () => {
	elapsed = 0;
	output.value = "";
	button.classList.add('is-loading');
	timer = setInterval(() => {
		elapsed += 300;
		if (elapsed > 10000) {
			console.log("Too long!");
			openModal(document.getElementById("toolong"));
			algo.terminate();
			clearInterval(timer);
			algo = new Worker('script.js');
			algo.addEventListener('message', (e) => {
				console.log("Work done");
				console.log(e.data);
				output.value = e.data;
				writeOutput(e.data);
				clearInterval(timer);
			});
			button.classList.remove('is-loading');
		}
	}, 300);
	algo.postMessage(["start", input.value]);
});

function writeOutput(val) {
	output.value = val;
	button.classList.remove('is-loading');
	button.innerHTML = "Get prime factorization!";
}

